package model;

public class Funcionario {
    private String Nome;
    private String CPF;
    private String Departamento;
    private String Telefone;
    private String resFunc;
    public Funcionario(){
    }
    public Funcionario(String Tel, String Nome, String CPF, String Departamento) {
        this.Telefone = Tel;
        this.Nome = Nome;
        this.CPF = CPF;
        this.Departamento = Departamento;
    }

    public String getTelefone() {
        return Telefone;
    }

    public void setTelefone(String Telefone) {
        this.Telefone = Telefone;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getDepartamento() {
        return Departamento;
    }

    public void setDepartamento(String Departamento) {
        this.Departamento = Departamento;
    }

    public String getResFunc() {
        return resFunc;
    }

    public void setResFunc(String resFunc) {
        this.resFunc = resFunc;
    }
    
    
}
