package model;

public class Login_and_CadModel {
    private String user;
    private String senha;
    
    private String userCad;
    private String senhaCad;
    private String confirmSenha;
    
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getUserCad() {
        return userCad;
    }

    public void setUserCad(String userCad) {
        this.userCad = userCad;
    }

    public String getSenhaCad() {
        return senhaCad;
    }

    public void setSenhaCad(String senhaCad) {
        this.senhaCad = senhaCad;
    }

    public String getConfirmSenha() {
        return confirmSenha;
    }

    public void setConfirmSenha(String confirmSenha) {
        this.confirmSenha = confirmSenha;
    }
    
}
