package controller; // pacote onde está a classe principalController

import model.Departamento;
import model.Funcionario;

/**
 *
 * @author Victor e Nicolly
 */
public class principalController { // Classe feita para condições
    FuncionarioTM funcTM = new FuncionarioTM();
    DepartamentoTM depTM = new DepartamentoTM();
    Funcionario f = new Funcionario();
    Departamento d = new Departamento();
    
    public boolean verificarCampos(Funcionario f){
        if(f.getNome().equals("") || f.getCPF().equals("") || f.getTelefone().equals("")){
            return true;
        }else{
            return false;
        }
    }
    public boolean verificarCamposDep(Departamento d){
        if(d.getCodigo().equals("") || d.getNomeDep().equals("")){
            return true;
        }else{
            return false;
        }
    }
    public boolean verificarResFuncNovo(Funcionario f){
        if(f.getResFunc().equals("novo")){
            return true;
        }else{
            return false;
        }
    }
    public boolean verificarResFuncEdit(Funcionario f){
        if(f.getResFunc().equals("editar")){
            return true;
        }else{
            return false;
        }
    }
     public boolean verificarResDepNovo(Departamento d){
        if(d.getResDep().equals("novo")){
            return true;
        }else{
            return false;
        }
    }
      public boolean verificarResDepEdit(Departamento d){
        if(d.getResDep().equals("editar")){
            return true;
        }else{
            return false;
        }
    }
    
    public String addItem(Departamento d){
        return d.getNomeDep();
    }
    
}

