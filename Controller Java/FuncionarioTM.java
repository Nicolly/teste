package controller; // pacote onde está nossa classe DepartamentoTM
//IMPORTAÇÕES: 
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.*;

public class FuncionarioTM extends AbstractTableModel{  // FuncionarioTM é herdeira de todos os recursos do AbstractTableModel
     
    private List<Funcionario> linhas;
    private String[] colunas = new String[]{"Nome","CPF","Depart.", "Tel."}; //Array responsável por definir o nome das colunas, onde cada nome vira uma coluna na tabela
    
    public FuncionarioTM() {
        linhas = new ArrayList<Funcionario>(); // instancia de um arrayList em lista
    }
    
    public FuncionarioTM(List<Funcionario> lista) {
        linhas = new ArrayList<Funcionario>(lista);
    }
    
    @Override
    public int getColumnCount() {  //Definição das quantidades de colunas
        return colunas.length; // A quantidade de colunas será o seu próprio tamanho
    }
    
    @Override
    public int getRowCount() { //Definição das quantidades de linhas
        return linhas.size(); /// A quantidade de linhas será o seu próprio tamanho
    }
     /*
     * Método responsável por nomear as colunas da tabela
     * @param int column
     * @return colunas[column]
     */
    @Override
    public String getColumnName(int columnIndex) { //Retorno das colunas nomeadas respectivamente pelo conteúdo do array colunas 
        return colunas[columnIndex];
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) { // pegar os valores da tabela
        Funcionario f = linhas.get(rowIndex);

        switch (columnIndex) {
            case 0: // primeira coluna
                return f.getNome(); /// retorna o Nome
            case 1: // segunda coluna
                return f.getCPF(); /// retorna o CPF
            case 2: // terceira coluna
                return f.getDepartamento(); /// retorna o Departamento
            case 3: // quarta coluna
                return f.getTelefone(); /// retorna o telefone
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }
    
    @Override
    //modifica na linha e coluna especificada
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Funcionario f = linhas.get(rowIndex); // Carrega o item da linha que deve ser modificado

        switch (columnIndex) { // Seta o valor do campo respectivo
            case 0:
                f.setNome(aValue.toString()); // modifica o Nome do Func
                break;
            case 1:
                f.setCPF(aValue.toString()); // modifica o CPF
                break;
            case 2:
                f.setDepartamento(aValue.toString()); // modifica o Departamento selecionado
            case 3:
                f.setTelefone(aValue.toString()); // modifica o numero de telefone
            default:
                // Isto não deveria acontecer...             
        }
        fireTableCellUpdated(rowIndex, columnIndex);
     }
    
    //modifica na linha especificada
    public void setValueAt(Funcionario aValue, int rowIndex) {
        Funcionario f = linhas.get(rowIndex); // Carrega o item da linha que deve ser modificado
       
        f.setNome(aValue.getNome());
        f.setCPF(aValue.getCPF());
        f.setDepartamento(aValue.getDepartamento());
        f.setTelefone(aValue.getTelefone());
        
        fireTableCellUpdated(rowIndex, 0);
        fireTableCellUpdated(rowIndex, 1);
        fireTableCellUpdated(rowIndex, 2);
        fireTableCellUpdated(rowIndex, 3);
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    
    public void addFuncionario(Funcionario f) {
        // Adiciona o registro.
        linhas.add(f);
        int ultimoIndice = getRowCount() - 1;
        fireTableRowsInserted(ultimoIndice, ultimoIndice);
    }
     /* Remove a linha especificada. */
    public void remove(int indiceLinha) {
        linhas.remove(indiceLinha);
        fireTableRowsDeleted(indiceLinha, indiceLinha);
    }

    /* Remove todos os registros. */
    public void limpar() {
        linhas.clear();
        fireTableDataChanged();
    }
 
}
