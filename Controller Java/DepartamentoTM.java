package controller; // pacote onde está nossa classe DepartamentoTM

// IMPORTAÇÕES : 
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.Departamento;

public class DepartamentoTM extends AbstractTableModel{ // DepartamentoTM é herdeira de todos os recursos do AbstractTableModel
    private List<Departamento> linhas; 
    private String[] colunas = new String[]{"Codigo.","Nome do Departamento"};  //Array responsável por definir o nome das colunas, onde cada nome vira uma coluna na tabela
    
    public DepartamentoTM() {
        linhas = new ArrayList<Departamento>(); // instancia de um arrayList em lista
    }
    
    public DepartamentoTM(List<Departamento> lista) {
        linhas = new ArrayList<Departamento>(lista);
    }
    
    @Override
    public int getColumnCount() {  //Definição das quantidades de colunas   
        return colunas.length; // A quantidade de colunas será o seu próprio tamanho
    }
    
    @Override
    public int getRowCount() { //Definição das quantidades de linhas
        return linhas.size(); /// A quantidade de linhas será o seu próprio tamanho
    }
    
	/*
	 * Método responsável por nomear as colunas da tabela
	 * @param int column
	 * @return colunas[column]
	 */
    @Override
    public String getColumnName(int columnIndex) { //Retorno das colunas nomeadas respectivamente pelo conteúdo do array colunas 
        return colunas[columnIndex];
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) { // pegar os valores da tabela
        Departamento d = linhas.get(rowIndex);
            
        switch (columnIndex) {
            case 0: // primeira coluna
                return d.getCodigo(); /// retorna o codigo
            case 1: // segunda coluna
                return d.getNomeDep(); /// retorna o nome do Departamento
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }
    
    @Override
    //modifica na linha e coluna especificada
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Departamento d = linhas.get(rowIndex); // Carrega o item da linha que deve ser modificado

        switch (columnIndex) { // Seta o valor do campo respectivo
            case 0:
                d.setCodigo(aValue.toString()); // modifica o codigo
                break;
            case 1:
                d.setNomeDep(aValue.toString()); // modifica o nome do Departamento
                break;
            default:
                // Isto não deveria acontecer...             
        }
        fireTableCellUpdated(rowIndex, columnIndex);
     }
    
    //modifica na linha especificada
    public void setValueAt(Departamento aValue, int rowIndex) {
        Departamento d = linhas.get(rowIndex); // Carrega o item da linha que deve ser modificado
        
        d.setCodigo(aValue.getCodigo());
        d.setNomeDep(aValue.getNomeDep());
        
        fireTableCellUpdated(rowIndex, 0);
        fireTableCellUpdated(rowIndex, 1);
        fireTableCellUpdated(rowIndex, 2);
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    
    public Departamento getDepartamento(int indiceLinha) {
        return linhas.get(indiceLinha);
    }
    
    public void addDepartamento(Departamento d) {
        // Adiciona o registro.
        linhas.add(d);
        int ultimoIndice = getRowCount() - 1;
        fireTableRowsInserted(ultimoIndice, ultimoIndice);
    }
     /* Remove a linha especificada. */
    public void remove(int indiceLinha) {
        linhas.remove(indiceLinha);
        fireTableRowsDeleted(indiceLinha, indiceLinha);
    }

    /* Remove todos os registros. */
    public void limpar() {
        linhas.clear();
        fireTableDataChanged();
    }
    
}
